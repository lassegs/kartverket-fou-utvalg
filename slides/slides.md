<!-- .slide: data-background="https://www.etpsmr.org/wp-content/uploads/2021/06/Horizon-Europe.png" data-background-size="33%" data-background-position="bottom" -->

# Kartverket og Horisont Europa

HEU, CL3 og norsk geokompetanse

Note:


- Er åpen for spørsmål gjennom det hele presentasjonen.
- Ikke noe her er hogget i stein. La oss skape denne sesjonen sammen. Jeg har litt opplegg, men sendes gjerne en retning dere synes er mer interessant.
- Skru gjerne av Mute.

---

## Om meg


<div class="halfsplit">
<div class="gridleft"> 

<img class="r-stretch" src="kartverket-pres-qr.svg">

https://lassegs.gitlab.io/kartverket-fou-utvalg/

</div>
<div class="gridright">

**Lasse Gullvåg Sætre**

[Enabling technologies](https://www.forskningsradet.no/portefoljer/muliggjorende-teknologier/)

lgs@rcn.no

[Forskningsrådet](forskningsradet.no)

NCP Horizon CL3 & DIGITAL, NCC-NO, ++

</div>
</div>

Note:

- Si litt om bakgrunn og min motivasjon for dette.
- IT-nerd som ville studere samfunnsfag, fant GIS.
- Startet MaptimeOSL - web mapping.
- Jobbet med jordobservasjon i FN.
- Norsk geokompetanse svært høy, verden har mye å tjene på at Kartverket blir mer utadvendte. 

---

![Pillars](https://www.zenit.de/wp-content/uploads/News/Struktur-Horizon-Europe.jpg)

---

## Norske søkere i HEU

<i class="ph-bold ph-currency-eur"></i> 488 mill. <small>(prosjektverdi 3.5mrd €)</small>

<i class="ph-bold ph-chart-pie-slice"></i> 3.35 % returandel <small>(Mål: 2.8 %)</small>

<i class="ph-bold ph-flag"></i> 23.6 % norsk suksessrate <small>(Europa: 16.2 %)</small>

<i class="ph-bold ph-medal"></i> 10 % av innstilte søknader

---

<!-- .slide: data-auto-animate -->

### Destinasjoner

HEUCL3: Civil Security for Society


- [Better protect the EU and its Citizens against Crime and Terrorism](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/better-protect-eu-and-its-citizens-against-crime-and-terrorism_en) ([Eksempel](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-fct-01-07?openForSubmission=false&programmePeriod=2021%20-%202027&frameworkProgramme=43108390&programmePart=43118971&pageSize=25))
- [Effective Management of External Borders](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/effective-management-eu-external-borders_en)([Eksempel](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-bm-01-02?openForSubmission=false&programmePeriod=2021%20-%202027&frameworkProgramme=43108390&programmePart=43118971&pageSize=50))
- [Resilient Infrastructures](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/resilient-infrastructures_en) ([Eksempel](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-infra-01-02?openForSubmission=false&programmePeriod=2021%20-%202027&frameworkProgramme=43108390&programmePart=43118971&pageSize=25))
- [Increased Cybersecurity](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/increased-cybersecurity_en) ([Eksempel](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-cs-01-01?openForSubmission=false&programmePeriod=2021%20-%202027&frameworkProgramme=43108390&programmePart=43118971&pageSize=25))
- [A Disaster Resilient Society for Europe](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/disaster-resilient-society-europe_en) ([Eksempel](https://ec.europa.eu/info/funding-tenders/opportunities/portal/screen/opportunities/topic-details/horizon-cl3-2024-drs-01-01?openForSubmission=false&programmePeriod=2021%20-%202027&frameworkProgramme=43108390&programmePart=43118971))
- [Strengthened Security Research and Innovation](https://rea.ec.europa.eu/funding-and-grants/horizon-europe-cluster-3-civil-security-society/strengthened-security-research-and-innovation-ssri_en)


Note:

- Crime&Terror: For å sikre at kriminalitet og terrorisme blir mer effektivt bekjempet - samtidig so grunnleggende rettigheter respekteres - takket være mer kraftfull forebygging, beredskap og respons.
- Borders: For å sikre at lovlige passasjerer og forsendelser enklere kan reise inn i EU, samtidig som ulovlig handel, menneskesmugling, piratkopiering, terrorisme og andre kriminelle handlinger forebygges, takket være forbedret grensekontroll på luft, land og sjø, samt økt maritim sikkerhet.
- Infrastructures: For å styrke motstandskraften og autonomien til fysiske og digitale infrastrukturer, og for å sikre at viktige samfunnsfunksjoner opprettholdes, takket være kraftigere forebygging, beredskap og respons.
- Cybersecurity: For økt cybersikkerhet og et tryggere nettbasert miljø ved å utvikle og benytte EU og medlemsstatenes kapasiteter.
- Disaster: Dette betyr at tap fra naturkatastrofer, ulykker og menneskeskapte katastrofer reduseres gjennom forbedret forebygging av katastroferisiko basert på preventive tiltak, bedre samfunnsberedskap og motstandskraft, samt forbedret katastroferisikohåndtering på en systematisk måte.
- SSRI: Å generere kunnskap og verdi innen tverrgående saker for å unngå sektorspesifikk skjevhet og bryte ned barrierer som hindrer spredningen av felles sikkerhetsløsninger. Det vil også støtte innovasjon og strategier for markedspenetrasjon.

---


## HEUCL3 2024

<i class="ph-bold ph-clock"></i> 27. juni - 20. nov

<i class="ph-bold ph-currency-eur"></i> 196 mill. euro

<i class="ph-bold ph-lightbulb"></i> 25 utlysninger (topics)

<!-- .slide: data-background="https://www.etpsmr.org/wp-content/uploads/2021/06/Horizon-Europe.png" data-background-size="33%" data-background-position="bottom" -->

---

### Hvorfor delta?

- Finansiering
- Kompetanseheving
- Teknologiutvikling
- Standardisering
- Internasjonalt samarbeid
- Lage og tilby opplæring
- Synlighet og omdømme

Note:

- Pengene skal man ikke blåse i. Økte driftbudsjetter. Typis finansiering til et prosjekt er mellom €3-20M.
- Kompetanseheving for organisasjon. Deltakelse gir unik innsikt og kompetanse. Styrker faglig kapasitet og !!Kalibrering av kompetanse!!
- Bedre rustet til å håndtere dagens, og fremtidens, utfordringer, gjennom Innovasjon og teknologi.
- Standardisering, men også regulering. Mye av politikkutviklingen i Norge skjer på bakgrunn av trender i Europa, og hvor tar EU inspirasjon fra? Horisont.
- Deltakelse i eksperimenter, disseminering og trening er ofte finansiert gjennom Horisont.
- En sjans til å være blant de første i verden til å gjøre noe. Ledende på noe. 



---

## Gode sjanser for offentlig sektor

- 3 % av returandel til offentlig sektor <small>(alle klynger)</small>
- Attraktiv samarbeidspartner
- I CL3 ofte påkrevd

![firstresponder](https://i.imgur.com/4Y5QLjs.png)

---

## Modus

<div class="halfsplit">
<div class="gridleft"> 

#### Roller i prosjektet

1. Leder, pådriver, problemutformer
2. Arbeidspakkeleder
3. Delta inn i arbeidspakke

</div>
<div class="gridright">

#### Innganger

1. <small>Egne problemstillinger opp mot tema i utlysning</small>
2. <small>Henvendelser fra andre miljøer som trenger partnere, sluttbrukere</small>

</div>
</div>

---

<!-- .slide: data-background-color="blue" -->

## <i class="ph ph-chat-circle-text"></i>

- "Next level"
- "Gøy", "interessant", "utfordrendre"
- "Grusomt", "veldig vanskelig", "selvtillitsdreper", "aner ikke hva jeg driver med"



---

<!-- .slide: data-background-color="black" -->


## Forskningsrådets kurstilbud

Forskningsrådet arrangerer kurs for deltakere, koordinatorer og administratorer på alle nivåer.

- [Åpne kaféer](https://www.forskningsradet.no/arrangementer/)
- [HEU Financial Audits](https://www.forskningsradet.no/arrangementer/2023/horizon-europe2020-master-of-finance-and-ec-audits-09-nov23/) 9. november
- [HEU Looking for partners and joining consortia (short webinar)](https://www.forskningsradet.no/arrangementer/2023/a2222.3---looking-for-partners-and-joining-consortia-short-webinar-14-nov/) 14. november
- [HEU - Prosjektutvikling 1](https://www.forskningsradet.no/arrangementer/2023/horisont-europa---prosjektutvikling-1/) 22. november


---

<!-- .slide: data-background-video="https://anti-brands.fra1.digitaloceanspaces.com/brands/forskningsradet/media/NFR_INTROPOSTER_LONG_NAMETAG_2022-05-05-074407_pwrq.mp4" data-background-opacity="1"-->
